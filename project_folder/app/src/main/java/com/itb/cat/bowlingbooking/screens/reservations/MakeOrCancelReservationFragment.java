package com.itb.cat.bowlingbooking.screens.reservations;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.itb.cat.bowlingbooking.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MakeOrCancelReservationFragment extends Fragment {

    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.makeResevation)
    Button makeResevation;
    @BindView(R.id.cancelReservation)
    Button cancelReservation;
    private MakeOrCancelReservationViewModel mViewModel;

    public static MakeOrCancelReservationFragment newInstance() {
        return new MakeOrCancelReservationFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.make_or_cancel_reservation_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MakeOrCancelReservationViewModel.class);
        // TODO: Use the ViewModel
    }

    @OnClick({R.id.makeResevation, R.id.cancelReservation})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.makeResevation:
                navigate(R.id.goToMakeReservation);
                break;
            case R.id.cancelReservation:
                break;
        }
    }

    private void navigate(int option) {
        Navigation.findNavController(getView()).navigate(option);

    }
}
