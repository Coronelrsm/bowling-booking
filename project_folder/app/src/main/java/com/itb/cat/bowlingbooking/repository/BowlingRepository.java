package com.itb.cat.bowlingbooking.repository;

import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.itb.cat.bowlingbooking.model.Reservation;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class BowlingRepository {

    private DatabaseReference bowlingDbReference;
    Reservation bowlingReservation;
    private static final int COST_PER_UNIT = 7;
    private static final double IVA = 1.21;

    public void addReservation() {
        bowlingDbReference = FirebaseDatabase.getInstance().getReference().child("Reservation"); //si no troba la taula, la crea


        bowlingReservation = new Reservation();
        bowlingReservation.setId(1);
        bowlingReservation.setUserName("Roger");
        bowlingReservation.setAlleyNumber(3);
        List<String> players = new ArrayList<>();
        players.add("Roger");
        players.add("Marta");
        players.add("Albert");
        players.add("Joan");
        bowlingReservation.setPlayers(players);
        bowlingReservation.setTime("18:00-18:30");

        double reservationCost = Math.round((players.size()*COST_PER_UNIT*IVA) * 100.0) / 100.0; //arrodonir el cost a 2 decimals sense double format

        bowlingReservation.setReservationCost(reservationCost);

        bowlingDbReference.push().setValue(bowlingReservation);

    }
}
