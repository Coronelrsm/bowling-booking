package com.itb.cat.bowlingbooking.screens.reservations;

import androidx.lifecycle.ViewModel;

import com.itb.cat.bowlingbooking.repository.BowlingRepository;

public class ReservationViewModel extends ViewModel {

    private BowlingRepository bowlingRepository;

    public void addReservation() {
        if(bowlingRepository==null) {
            bowlingRepository = new BowlingRepository();
        }

        bowlingRepository.addReservation();

    }

}
