package com.itb.cat.bowlingbooking.screens.loginscreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.itb.cat.bowlingbooking.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginFragment extends Fragment {

    @BindView(R.id.appIcon)
    ImageView appIcon;
    @BindView(R.id.userName)
    EditText userName;
    @BindView(R.id.userPassword)
    EditText userPassword;
    @BindView(R.id.loginButton)
    Button loginButton;
    @BindView(R.id.textOr)
    TextView textOr;
    @BindView(R.id.textSignIn)
    TextView textSignIn;
    @BindView(R.id.googleButton)
    ImageButton googleButton;
    @BindView(R.id.facebookButton)
    ImageButton facebookButton;
    @BindView(R.id.twitterButton)
    ImageButton twitterButton;
    @BindView(R.id.myCompany)
    TextView myCompany;
    private LoginViewModel mViewModel;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
    }

    @OnClick({R.id.loginButton, R.id.googleButton, R.id.facebookButton, R.id.twitterButton})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.loginButton:
                navigateToMainMenu();
                break;
            case R.id.googleButton:
                break;
            case R.id.facebookButton:
                break;
            case R.id.twitterButton:
                break;
        }
    }

    private void navigateToMainMenu() {
        //NavDirections action = LoginFragmentDirections.goToMainMenu();
        Navigation.findNavController(getView()).navigate(R.id.goToMainMenu);

    }
}
