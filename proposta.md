# Bowling Reservation
### Alumne: Roger Serra
### Link Figma: https://www.figma.com/file/am8DDsZYXLYUB3CyvgTLvk/Bowling-Reservation?node-id=4%3A0
### Descripció de la proposta
El procés de reservar una pista per jugar és molt repetitiu. A més, hi ha llargues cues d'espera si no hi ha reserva i trucar és menys còmode que una app.
La meva idea és **automatitzar** aquest procés, fent que l'usuari pugui veure les pistes de bitlles i els horaris que tenen reservades al llarg del dia, com la cita prèvia del DNI. El públic objectiu seria la gent entre 15-40 anys, que són els que solen acudir a aquests llocs d'entreteniment.
També tinc pensat que es puguin afegir amics a la app, de tal manera que en afegir les persones a la partida, el seu nom serà el que estarà a la pantalla juntament amb la puntuació. També tindran un atribut número de calçat, per tal que els empleats els puguin tenir preparats un cop arribin els clients.
També tinc pensat, opcionalment, poder afegir begudes i aperitius al ticket que genera la app, per tal de **fer més còmoda l'estància dels clients** a la pista de bitlles.
**La meva idea és que els jugadors arribin a la pista de bitlles i ho tinguin tot enllestit**: recollir el calçat, demanar els refrigeris i accedir a la pista.
A més, aquesta app també serviria per **fidelitzar clients**, podent oferir promocions, vals i descomptes.
En resum:
- **Registrar un perfil** (nom, email, direccio, targeta...)
- **Afegir amics recurrents** (per obtenir el nom que apareixerà a la pantalla i el número de calçat). No es demanaran dades personals més enllà del nom i el calçat, per temes de protecció de dades i privacitat.
- Veure **horaris** i **reservar pista** i **calçat**
- Opcionalment, **demanar refrigeris**
- Opcionalment, **historial de partides**


*Com a anotació, primer de tot havia pensat en una App de casino, però més que solucionar problemes, acabaria causant ludopatia.*
